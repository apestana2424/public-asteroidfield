﻿using System;
using System.Threading;

namespace AsteroidField
{
    internal class Program
    {
        /*
         * Possible new features:
         * - Have a limited number of ammunition, somehow giving the player the opportunity to
         *   pick up ammunition during the game.
         * - If the player presses <ESC> to exit, ask him for confirmation.
         * - Gradually increase the number of asteroids and/or their speed as time
         *   goes by.
         * - Each time the number of asteroids and/or speed increases,
         *   it can be considered that the player has passed the level and
         *   info is given to the player on which level he is in.
         * -- Perform an animation when the player passes the level.
         * - Have other types of ammunition available (for example, a bomb that destroys
         *   all asteroids on the playing field, or ammunition in which 3 projectiles are shot
         *   in parallel at the same time.).
         * - Perform an animation when an asteroid is destroyed.
         * - Make the player lose a life if he lets a certain number
         *   of asteroids through the playing field without destroying them.
         * - Have an intro screen to the game, in which a message is displayed and
         *   a music is played.
         * - Somehow, give the player the opportunity to get more lives during the game.
         */

        private const int WINDOW_WIDTH = 40;
        private const int WINDOW_HEIGHT = 20;

        private const int GAME_AREA_MAX_X = WINDOW_WIDTH - 20;
        private const int GAME_AREA_MAX_Y = WINDOW_HEIGHT - 2;

        private const int ASTEROID_PROBABILITY = 5; // Up to 100 (%).
        private const int ASTEROID_SPEED_REDUCTION = 6; // Move the asteroids every ASTEROID_SPEED_REDUCTION cycles of the main game cycle.

        private const int MAIN_GAME_CYCLE_SLEEP_DURATION = 50; // For how many milliseconds main game cycle sleeps.

        private const int ASTEROID_DESTRUCTION_SCORE = 10;

        private const int NUMBER_OF_LIVES = 3;

        private static void Main(string[] args)
        {
            ConfigureConsole();

            DrawInformationPanel();

            char[,] currentGameArea = new char[GAME_AREA_MAX_X + 1, GAME_AREA_MAX_Y + 1];
            char[,] futureGameArea = new char[GAME_AREA_MAX_X + 1, GAME_AREA_MAX_Y + 1];

            SetGameAreaToSpaces(currentGameArea);
            SetGameAreaToSpaces(futureGameArea);

            InitializeGameArea(futureGameArea);

            int score = 0;
            int lives = NUMBER_OF_LIVES;
            int moveAsteroidsCounter = 0;
            Random randomNumberGenerator = new Random();
            while (lives > 0)
            {
                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo key = Console.ReadKey(true);

                    switch (key.Key)
                    {
                        case ConsoleKey.LeftArrow:
                            MoveSpaceship(-1, futureGameArea);
                            break;

                        case ConsoleKey.RightArrow:
                            MoveSpaceship(1, futureGameArea);
                            break;

                        case ConsoleKey.Spacebar:
                            Shoot(futureGameArea);
                            break;

                        case ConsoleKey.Escape:
                            return;

                        default:
                            break;
                    }
                }

                if (DecideIfAsteroidAppears(randomNumberGenerator))
                {
                    GenerateAsteroid(futureGameArea, randomNumberGenerator);
                }

                int addToScore;
                int lostLives;
                AdvanceState(currentGameArea, futureGameArea, moveAsteroidsCounter == 0, out addToScore, out lostLives);
                score += addToScore;
                lives -= lostLives;

                RefreshGameAreaDraw(currentGameArea, futureGameArea);
                RefreshInformationPanelDraw(score, lives);

                moveAsteroidsCounter = (moveAsteroidsCounter + 1) % ASTEROID_SPEED_REDUCTION;

                Thread.Sleep(MAIN_GAME_CYCLE_SLEEP_DURATION);
            }

            Console.ForegroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("+++++++++++++++++++++");
            Console.WriteLine("+    GAME OVER!     +");
            Console.WriteLine("+   Press <ENTER>   +");
            Console.WriteLine("+     to exit.      +");
            Console.WriteLine("+++++++++++++++++++++");
            Console.ResetColor();
            Console.ReadLine();
        }

        private static void ConfigureConsole()
        {
            Console.CursorVisible = false;
            Console.SetWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
            Console.SetBufferSize(Console.WindowWidth, Console.WindowHeight);
            Console.Title = "Asteroid Field";
        }

        private static void DrawInformationPanel()
        {
            Console.ForegroundColor = ConsoleColor.Blue;

            for (int i = 1; i < Console.WindowHeight - 1; i++)
            {
                Console.SetCursorPosition(GAME_AREA_MAX_X + 1, i);
                Console.Write("|");
            }

            Console.SetCursorPosition(GAME_AREA_MAX_X + 5, 2);
            Console.Write("-Score-");

            Console.SetCursorPosition(GAME_AREA_MAX_X + 5, 5);
            Console.Write("-Lives-");

            Console.SetCursorPosition(GAME_AREA_MAX_X + 5, 8);
            Console.Write("-To Play-");
            Console.SetCursorPosition(GAME_AREA_MAX_X + 6, 9);
            Console.Write("Left Arrow");
            Console.SetCursorPosition(GAME_AREA_MAX_X + 6, 10);
            Console.Write("Right Arrow");
            Console.SetCursorPosition(GAME_AREA_MAX_X + 6, 11);
            Console.Write("Space");

            Console.ResetColor();
        }

        private static void RefreshInformationPanelDraw(int score, int lives)
        {
            Console.ForegroundColor = ConsoleColor.Blue;

            Console.SetCursorPosition(GAME_AREA_MAX_X + 6, 3);
            Console.Write("{0}  ", score);

            Console.SetCursorPosition(GAME_AREA_MAX_X + 6, 6);
            Console.Write("{0}  ", lives);

            Console.ResetColor();
        }

        private static void SetGameAreaToSpaces(char[,] gameArea)
        {
            for (int i = 0; i < gameArea.GetLength(0); i++)
            {
                for (int j = 0; j < gameArea.GetLength(1); j++)
                {
                    gameArea[i, j] = ' ';
                }
            }
        }

        private static void InitializeGameArea(char[,] gameArea)
        {
            int halfWidth = gameArea.GetLength(0) / 2;
            gameArea[halfWidth - 1, gameArea.GetLength(1) - 1] = '^';
        }

        private static bool DecideIfAsteroidAppears(Random randomNumberGenerator)
        {
            int num = randomNumberGenerator.Next(100);
            return num < ASTEROID_PROBABILITY;
        }

        private static void GenerateAsteroid(char[,] gameArea, Random randomNumberGenerator)
        {
            int x = randomNumberGenerator.Next(GAME_AREA_MAX_X + 1);
            if (gameArea[x, 0] == ' ')
            {
                gameArea[x, 0] = '*';
            }
        }

        // If direction >= 0, move to the right.
        // If direction < 0, move to the left.
        private static void MoveSpaceship(int direction, char[,] gameArea)
        {
            for (int i = 0; i < gameArea.GetLength(0); i++)
            {
                if (gameArea[i, gameArea.GetLength(1) - 1] == '^')
                {
                    // Move to the right.
                    if (direction >= 0 && (i + 1) < gameArea.GetLength(0))
                    {
                        gameArea[i, gameArea.GetLength(1) - 1] = ' ';
                        gameArea[i + 1, gameArea.GetLength(1) - 1] = '^';
                    }

                    // Move to the left.
                    if (direction < 0 && (i - 1) >= 0)
                    {
                        gameArea[i, gameArea.GetLength(1) - 1] = ' ';
                        gameArea[i - 1, gameArea.GetLength(1) - 1] = '^';
                    }

                    return;
                }
            }
        }

        private static void Shoot(char[,] gameArea)
        {
            for (int i = 0; i < gameArea.GetLength(0); i++)
            {
                if (gameArea[i, gameArea.GetLength(1) - 1] == '^')
                {
                    gameArea[i, gameArea.GetLength(1) - 2] = '.';

                    return;
                }
            }
        }

        private static void AdvanceState(char[,] currentGameArea, char[,] futureGameArea, bool moveAsteroids, out int score, out int lostLives)
        {
            score = 0;
            lostLives = 0;

            for (int i = 0; i < currentGameArea.GetLength(0); i++)
            {
                for (int j = 0; j < currentGameArea.GetLength(1); j++)
                {
                    switch (currentGameArea[i, j])
                    {
                        case '.':
                            futureGameArea[i, j] = ' ';
                            if (j > 0)
                            {
                                if (futureGameArea[i, j - 1] == ' ')
                                {
                                    futureGameArea[i, j - 1] = '.';
                                }
                                else if (futureGameArea[i, j - 1] == '*')
                                {
                                    futureGameArea[i, j - 1] = ' ';
                                    score += ASTEROID_DESTRUCTION_SCORE;
                                }
                            }
                            break;

                        case '*':
                            if (moveAsteroids)
                            {
                                futureGameArea[i, j] = ' ';
                                if (j < futureGameArea.GetLength(1) - 1)
                                {
                                    if (futureGameArea[i, j + 1] == ' ')
                                    {
                                        futureGameArea[i, j + 1] = '*';
                                    }
                                    else if (futureGameArea[i, j + 1] == '.')
                                    {
                                        futureGameArea[i, j + 1] = ' ';
                                        score += ASTEROID_DESTRUCTION_SCORE;
                                    }
                                    else if (futureGameArea[i, j + 1] == '^')
                                    {
                                        lostLives += 1;
                                        Console.Beep();
                                    }
                                }
                            }
                            break;
                    }
                }
            }
        }

        private static void RefreshGameAreaDraw(char[,] currentGameArea, char[,] futureGameArea)
        {
            for (int i = 0; i < currentGameArea.GetLength(0); i++)
            {
                for (int j = 0; j < currentGameArea.GetLength(1); j++)
                {
                    if (currentGameArea[i, j] != futureGameArea[i, j])
                    {
                        Console.SetCursorPosition(i, j);
                        switch (futureGameArea[i, j])
                        {
                            case '^':
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;

                            case '.':
                                Console.ForegroundColor = ConsoleColor.Green;
                                break;

                            case '*':
                                Console.ForegroundColor = ConsoleColor.Red;
                                break;
                        }
                        Console.Write(futureGameArea[i, j]);
                        Console.ResetColor();
                        currentGameArea[i, j] = futureGameArea[i, j];
                    }
                }
            }
        }
    }
}